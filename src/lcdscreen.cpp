#include <lcdscreen.hpp>

LcdScreen::LcdScreen()
{
	motor_instance = Motor::getInstance();
	sensor_instance = Sensor::getInstance();
}

LcdScreen *LcdScreen::getInstance()
{
	if (!s_instance)
		s_instance = new LcdScreen();
	return s_instance;
}

void LcdScreen::update()
{
	// TODO replace test1 with sensor->getDataStr() or something like it
	display.clear();	// Clear the previous screen
	char line[21] = "";	// String buffer that will contain each line before printing them
	char test1[6] = "100.0";
	char bufferCurrent[6];
	char formatPWD[18] = "XX %s%% PID: %s%%"; 

	// Motor States (Target and Current)
	// Front Left
	motor_instance->getPwmFL(bufferCurrent);
	formatPWD[0] = 'F';
	formatPWD[1] = 'L';
	sprintf(line, formatPWD, bufferCurrent, test1);
	display.print(line, START_LINE_X, LINE0_Y, &defaultFont);

	// Front Right
	motor_instance->getPwmFR(bufferCurrent);
	formatPWD[0] = 'F';
	formatPWD[1] = 'R';
	sprintf(line, formatPWD, bufferCurrent, test1);
	display.print(line, START_LINE_X, LINE1_Y, &defaultFont);

	// Rear Left
	motor_instance->getPwmRL(bufferCurrent);
	formatPWD[0] = 'R';
	formatPWD[1] = 'L';
	sprintf(line, formatPWD, bufferCurrent, test1);
	display.print(line, START_LINE_X, LINE2_Y, &defaultFont);

	// Rear Right
	motor_instance->getPwmRR(bufferCurrent);
	formatPWD[0] = formatPWD[1] = 'R';
	sprintf(line, formatPWD, bufferCurrent, test1);
	display.print(line, START_LINE_X, LINE3_Y, &defaultFont);
	// end of motor States

	// Sensors
	char bufferX[7];
	char bufferY[7];
	char bufferZ[7];
	sensor_instance->getGyroX(bufferX);
	sensor_instance->getGyroY(bufferY);
	sensor_instance->getGyroZ(bufferZ);
	sprintf(line, "G%s/%s/%s", bufferX, bufferY, bufferZ);
	display.print(line, START_LINE_X, LINE5_Y, &defaultFont);

	sensor_instance->getAccelX(bufferX);
	sensor_instance->getAccelY(bufferY);
	sensor_instance->getAccelZ(bufferZ);
	sprintf(line, "A%s/%s/%s", bufferX, bufferY, bufferZ);
	display.print(line, START_LINE_X, LINE6_Y, &defaultFont);

	sensor_instance->getMagnetoX(bufferX);
	sensor_instance->getMagnetoY(bufferY);
	sensor_instance->getMagnetoZ(bufferZ);
	sprintf(line, "M%s/%s/%s", bufferX, bufferY, bufferZ);
	display.print(line, START_LINE_X, LINE7_Y, &defaultFont);
	// end of Sensors

	display.flush(); // Send modification to screen
}

LcdScreen *LcdScreen::s_instance = 0;