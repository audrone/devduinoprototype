#include <sensors.hpp>

// Private functions

Sensor::Sensor()
{
	gyro[0] = 0;
	gyro[1] = 0;
	gyro[2] = 0;
	accel[0] = 0;
	accel[1] = 0;
	accel[2] = 0;
	magneto[0] = 0;
	magneto[1] = 0;
	magneto[2] = 0;

	Wire.begin();
	setupSensor();
}

void Sensor::setupGyro()
{
	Wire.beginTransmission(MPU9250_AD);
	Wire.write(GYRO_CONFIG_REG);
	if (GYRO_SCALE == 250)
	{
		Wire.write(0x00);
	}
	else if (GYRO_SCALE == 500)
	{
		Wire.write(0x08);
	}
	else if (GYRO_SCALE == 1000)
	{
		Wire.write(0x010);
	}
	else if (GYRO_SCALE == 2000)
	{
		Wire.write(0x18);
	}
	Wire.endTransmission();

	Wire.beginTransmission(MPU9250_AD);
	Wire.write(CONFIG_REG);
	Wire.write(0x05);
	Wire.endTransmission();
}

void Sensor::setupAccel()
{
	Wire.beginTransmission(MPU9250_AD);
	Wire.write(ACCEL_CONFIG_1_REG);
	if (ACCEL_SCALE == 2)
	{
		Wire.write(0x00);
	}
	else if (ACCEL_SCALE == 4)
	{
		Wire.write(0x08);
	}
	else if (ACCEL_SCALE == 8)
	{
		Wire.write(0x010);
	}
	else if (ACCEL_SCALE == 16)
	{
		Wire.write(0x18);
	}
	Wire.endTransmission();

	Wire.beginTransmission(MPU9250_AD);
	Wire.write(ACCEL_CONFIG_2_REG);
	Wire.write(0x05);
	Wire.endTransmission();
}

void Sensor::setupMagneto()
{
	uint8_t *buffer = NULL;

	// First set up the MPU9250 for bypass multiplexer
	Wire.beginTransmission(MPU9250_AD);
	Wire.write(USER_CONTROL_REG);
	Wire.write(0x00); // Reset user control
	Wire.endTransmission();

	Wire.beginTransmission(MPU9250_AD);
	Wire.write(BYPASS_EN);
	Wire.write(0x02); // Enable multiplexer bypass
	Wire.endTransmission();

	// Now we fetch the axes asa, they are needed to process raw data and need
	// to be done before the config

	Wire.beginTransmission(AK8963_AD);
	Wire.write(CONTROL_1_REG); // Set the Magnetometer to fuse ROM to access asa
	Wire.write(0x1F);
	Wire.endTransmission();

	delay(100); // Wait for the changes to take effect

	Wire.beginTransmission(AK8963_AD);
	Wire.write(ASAX_REG);
	Wire.readBytes(buffer, 3);
	Wire.endTransmission();
	this->setAsax((((buffer[0] - 128) * 0.5) / 128) - 1); // Formula in documentation
	this->setAsay((((buffer[1] - 128) * 0.5) / 128) - 1); // Formula in documentation
	this->setAsaz((((buffer[2] - 128) * 0.5) / 128) - 1); // Formula in documentation

	// We need to set it in power down mode before reconfiguring it
	Wire.beginTransmission(AK8963_AD);
	Wire.write(CONTROL_1_REG);
	Wire.write(0x00);
	Wire.endTransmission();

	delay(100); // Wait for changes to take effect

	// The real configuration
	Wire.beginTransmission(AK8963_AD);
	Wire.write(CONTROL_1_REG);
	Wire.write(0x16); // This set the magneto to continuous mode 2 (100Hz) and 16 bit
	Wire.endTransmission();

	delay(100); // Wait for changes to take effect
}

void Sensor::readGyro()
{
	Wire.beginTransmission(MPU9250_AD);
	Wire.write(GYRO_XOUT_H);
	Wire.endTransmission();
	Wire.requestFrom(MPU9250_AD, 2);

	if (Wire.available() <= 2)
	{
		gyro[0] = ((Wire.read() * 16) + Wire.read()) / GYRO_SENS;
	}

	Wire.beginTransmission(MPU9250_AD);
	Wire.write(GYRO_YOUT_H);
	Wire.endTransmission();
	Wire.requestFrom(MPU9250_AD, 2);

	if (Wire.available() <= 2)
	{
		gyro[1] = ((Wire.read() * 16) + Wire.read()) / GYRO_SENS;
	}

	Wire.beginTransmission(MPU9250_AD);
	Wire.write(GYRO_ZOUT_H);
	Wire.endTransmission();
	Wire.requestFrom(MPU9250_AD, 2);

	if (Wire.available() <= 2)
	{
		gyro[2] = ((Wire.read() * 16) + Wire.read()) / GYRO_SENS;
	}
}

void Sensor::readAccel()
{
	Wire.beginTransmission(MPU9250_AD);
	Wire.write(ACCEL_XOUT_H);
	Wire.endTransmission();

	Wire.requestFrom(MPU9250_AD, 2);

	if (Wire.available() <= 2)
	{
		accel[0] = ((Wire.read() * 16) + Wire.read()) / ACCEL_SENS;
	}

	Wire.beginTransmission(MPU9250_AD);
	Wire.write(ACCEL_YOUT_H);
	Wire.endTransmission();

	Wire.requestFrom(MPU9250_AD, 2);

	if (Wire.available() <= 2)
	{
		accel[1] = ((Wire.read() * 16) + Wire.read()) / ACCEL_SENS;
	}

	Wire.beginTransmission(MPU9250_AD);
	Wire.write(ACCEL_ZOUT_H);
	Wire.endTransmission();

	Wire.requestFrom(MPU9250_AD, 2);

	if (Wire.available() <= 2)
	{
		accel[2] = ((Wire.read() * 16) + Wire.read()) / ACCEL_SENS;
	}
}

void Sensor::readMagneto()
{
	uint16_t buffer = 0;

	Wire.beginTransmission(AK8963_AD);
	Wire.write(STATUS_1_REG);
	Wire.endTransmission();
	Wire.requestFrom(AK8963_AD, 1);
	buffer = Wire.read();
	if ((buffer | MASK_DATA_RDY) == MASK_DATA_RDY)
	{
		Wire.beginTransmission(AK8963_AD);
		Wire.write(HXL_AD);
		Wire.endTransmission();
		Wire.requestFrom(AK8963_AD, 2);

		uint16_t buffer0 = Wire.read();
		uint16_t buffer1 = Wire.read();

		Wire.beginTransmission(AK8963_AD);
		Wire.write(HYL_AD);
		Wire.endTransmission();
		Wire.requestFrom(AK8963_AD, 2);

		uint16_t buffer2 = Wire.read();
		uint16_t buffer3 = Wire.read();

		Wire.beginTransmission(AK8963_AD);
		Wire.write(HZL_AD);
		Wire.endTransmission();
		Wire.requestFrom(AK8963_AD, 3);

		uint16_t buffer4 = Wire.read();
		uint16_t buffer5 = Wire.read();
		uint16_t buffer6 = Wire.read();

		if (!(buffer6 | MAGIC_OVERFLOW))
		{
			magneto[0] = (buffer0 + (buffer1 * 16)) * MAGNETO_SCALE * asax;
			magneto[1] = (buffer2 + (buffer3 * 16)) * MAGNETO_SCALE * asay;
			magneto[2] = (buffer4 + (buffer5 * 16)) * MAGNETO_SCALE * asaz;
		}
	}
}

// Public functions

Sensor *Sensor::getInstance()
{
	if (!s_instance)
		s_instance = new Sensor();
	return s_instance;
}

void Sensor::setupSensor()
{
	setupGyro();
	setupAccel();
	setupMagneto();

	// Wake up device for all sensors
	Wire.beginTransmission(MPU9250_AD);
	Wire.write(PWR_MGMT_1);
	Wire.write(0x00); // set clock to internal
	Wire.endTransmission();

	Wire.beginTransmission(MPU9250_AD);
	Wire.write(PWR_MGMT_2);
	Wire.write(0x3F); // Enable gyro and accel on all axes
	Wire.endTransmission();
}

uint16_t Sensor::getAsax()
{
	return this->asax;
}

void Sensor::setAsax(int asax)
{
	this->asax = asax;
}

uint16_t Sensor::getAsay()
{
	return this->asay;
}

void Sensor::setAsay(int asay)
{
	this->asay = asay;
}

uint16_t Sensor::getAsaz()
{
	return this->asaz;
}

void Sensor::setAsaz(int asaz)
{
	this->asaz = asaz;
}

float Sensor::getGyroX()
{
	return this->gyro[0];
}
float Sensor::getGyroY()
{
	return this->gyro[1];
}
float Sensor::getGyroZ()
{
	return this->gyro[2];
}

float Sensor::getAccelX()
{
	return this->accel[0];
}
float Sensor::getAccelY()
{
	return this->accel[1];
}
float Sensor::getAccelZ()
{
	return this->accel[2];
}

float Sensor::getMagnetoX()
{
	return this->magneto[0];
}
float Sensor::getMagnetoY()
{
	return this->magneto[1];
}
float Sensor::getMagnetoZ()
{
	return this->magneto[2];
}

void Sensor::getGyroX(char outStr[6])
{
	dtostrf(gyro[0], TOTAL_DECIMAL_S, AFTER_DECIMAL_S, outStr);
}
void Sensor::getGyroY(char outStr[6])
{
	dtostrf(gyro[1], TOTAL_DECIMAL_S, AFTER_DECIMAL_S, outStr);
}
void Sensor::getGyroZ(char outStr[6])
{
	dtostrf(gyro[2], TOTAL_DECIMAL_S, AFTER_DECIMAL_S, outStr);
}

void Sensor::getAccelX(char outStr[6])
{
	dtostrf(accel[0], TOTAL_DECIMAL_S, AFTER_DECIMAL_S, outStr);
}
void Sensor::getAccelY(char outStr[6])
{
	dtostrf(accel[1], TOTAL_DECIMAL_S, AFTER_DECIMAL_S, outStr);
}
void Sensor::getAccelZ(char outStr[6])
{
	dtostrf(accel[2], TOTAL_DECIMAL_S, AFTER_DECIMAL_S, outStr);
}

void Sensor::getMagnetoX(char outStr[6])
{
	dtostrf(magneto[0], TOTAL_DECIMAL_S, AFTER_DECIMAL_S, outStr);
}
void Sensor::getMagnetoY(char outStr[6])
{
	dtostrf(magneto[1], TOTAL_DECIMAL_S, AFTER_DECIMAL_S, outStr);
}
void Sensor::getMagnetoZ(char outStr[6])
{
	dtostrf(magneto[2], TOTAL_DECIMAL_S, AFTER_DECIMAL_S, outStr);
}

void Sensor::update()
{
	readGyro();
	readAccel();
	readMagneto();
}

Sensor *Sensor::s_instance = 0;