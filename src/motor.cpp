#include <motor.hpp>

Motor::Motor()
{
	setupMotor();
}

Motor *Motor::getInstance()
{
	if (!s_instance)
		s_instance = new Motor();
	return s_instance;
}

void Motor::setupMotor()
{
	pinMode(PIN_MOTOR_FL, OUTPUT);
	pinMode(PIN_MOTOR_FR, OUTPUT);
	pinMode(PIN_MOTOR_RL, OUTPUT);
	pinMode(PIN_MOTOR_RR, OUTPUT);

	pwmFL = 0;
	pwmFR = 0;
	pwmRL = 0;
	pwmRR = 0;
}

int Motor::percentToDutyCycle(float pwm)
{
	int temp = ((pwm*255)/100);
	return temp;
}

float Motor::getPwmFL()
{
	return this->pwmFL;
}

float Motor::getPwmFR()
{
	return this->pwmFR;
}

float Motor::getPwmRL()
{
	return this->pwmRL;
}

float Motor::getPwmRR()
{
	return this->pwmRR;
}

void Motor::getPwmFL(char outStr[6])
{
	dtostrf(pwmFL, TOTAL_DECIMAL, AFTER_DECIMAL, outStr);
}

void Motor::getPwmFR(char outStr[6])
{
	dtostrf(pwmFR, TOTAL_DECIMAL, AFTER_DECIMAL, outStr);
}

void Motor::getPwmRL(char outStr[6])
{
	dtostrf(pwmRL, TOTAL_DECIMAL, AFTER_DECIMAL, outStr);
}

void Motor::getPwmRR(char outStr[6])
{
	dtostrf(pwmRR, TOTAL_DECIMAL, AFTER_DECIMAL, outStr);
}

void Motor::setPwmFL(float pwm)
{
	this->pwmFL = pwm;
	analogWrite(PIN_MOTOR_FL, this->percentToDutyCycle(pwm));
}

void Motor::setPwmFR(float pwm)
{
	this->pwmFR = pwm;
	analogWrite(PIN_MOTOR_FR, this->percentToDutyCycle(pwm));
}

void Motor::setPwmRL(float pwm)
{
	this->pwmRL = pwm;
	analogWrite(PIN_MOTOR_RL, this->percentToDutyCycle(pwm));
}

void Motor::setPwmRR(float pwm)
{
	this->pwmRR = pwm;
	analogWrite(PIN_MOTOR_RR, this->percentToDutyCycle(pwm));
}

Motor *Motor::s_instance = 0;