#include <stdlib.h>
#include <Arduino.h>

#include "lcdscreen.hpp"
#include <devduino.h>
//#include "motor.hpp"
//#include "sensors.hpp"

#define MPU9250 0b110100

using namespace std;
LcdScreen* screen;
Motor* motors;
Sensor* sensors;
float test = 0;

void setup() {
	while(!Serial);
	devduino.begin();
	screen = LcdScreen::getInstance();
	motors = Motor::getInstance();
	sensors = Sensor::getInstance();
}

void loop() {
	test += 0.1;
	if(test > 100)
	{
		test = 0;
	}
	motors->setPwmFL(test);
	sensors->update();
	screen->update();
}
	