#include <StandardCplusplus.h>
#include <devduino.h>
#include <Wire.h>

#define MPU9250_AD 0x68 // Gyro/Accel I2C adr
#define AK8963_AD 0b0100000 // Magnetometer I2C adr

// MPU9250 registeries
#define CONFIG_REG 0x1A
#define GYRO_CONFIG_REG 0x1B
#define ACCEL_CONFIG_1_REG 0x1C
#define ACCEL_CONFIG_2_REG 0x1D
#define BYPASS_EN 0x37
#define USER_CONTROL_REG 0x6A
#define PWR_MGMT_1 0x6B
#define PWR_MGMT_2 0x6C
#define GYRO_XOUT_H 0x43
#define GYRO_XOUT_L 0x44
#define GYRO_YOUT_H 0x45
#define GYRO_YOUT_L 0x46
#define GYRO_ZOUT_H 0x47
#define GYRO_ZOUT_L 0x48
#define ACCEL_XOUT_H 0x3B
#define ACCEL_XOUT_L 0x3C
#define ACCEL_YOUT_H 0x3D
#define ACCEL_YOUT_L 0x3E
#define ACCEL_ZOUT_H 0x3F
#define ACCEL_ZOUT_L 0x40

// AK8963 registeries
#define CONTROL_1_REG 0x0A
#define STATUS_1_REG 0x02
#define MAGIC_OVERFLOW 0x8
#define ASAX_REG 0x10
#define ASAY_REG 0x11
#define ASAZ_REG 0x12
#define HXL_AD 0x03		   // Low byte of the X-Axis
#define HXH_AD 0x04		   // High byte of the X-Axis
#define HYL_AD 0x05		   // Low Y
#define HYH_AD 0x06		   // High Y
#define HZL_AD 0x07		   // Low Z
#define HZH_AD 0x08		   // High Z
#define MASK_DATA_RDY 0x01 // Mask to check if magneto data is ready

// Sensors Sensibility/Scale
#define GYRO_SCALE 500	   // in ±4g
#define ACCEL_SCALE 4	   // in dps (degree per second)
#define MAGNETO_SCALE 4800 // Fixed
#define GYRO_SENS 65.5	   // Decided by table 1 Gyroscope Specifications
#define ACCEL_SENS 8.192   // Decided by table 2 Accelerometer Specifications
#define MAGNETO_SENS 0.6   // Decided by section 3.3, Magnetometer Specifications

// For float to char[] conversion
#define TOTAL_DECIMAL_S 6
#define AFTER_DECIMAL_S 2

class Sensor
{
	static Sensor *s_instance;
	int percentToDutyCycle(float pwm);
	uint16_t asax;
	uint16_t asay;
	uint16_t asaz;

	float gyro[3];	  // [0] = x, [1] = y, [2] = z
	float accel[3];	  // [0] = x, [1] = y, [2] = z
	float magneto[3]; // [0] = x, [1] = y, [2] = z

	Sensor();
	void setupGyro();
	void setupAccel();
	void setupMagneto();

	void setAsax(int asax);
	void setAsay(int asay);
	void setAsaz(int asaz);

	void readGyro();
	void readAccel();
	void readMagneto();

public:
	static Sensor *getInstance();

	void setupSensor();

	uint16_t getAsax();
	uint16_t getAsay();
	uint16_t getAsaz();

	float getGyroX();
	float getGyroY();
	float getGyroZ();

	float getAccelX();
	float getAccelY();
	float getAccelZ();

	float getMagnetoX();
	float getMagnetoY();
	float getMagnetoZ();

	void getGyroX(char outStr[6]);
	void getGyroY(char outStr[6]);
	void getGyroZ(char outStr[6]);

	void getAccelX(char outStr[6]);
	void getAccelY(char outStr[6]);
	void getAccelZ(char outStr[6]);

	void getMagnetoX(char outStr[6]);
	void getMagnetoY(char outStr[6]);
	void getMagnetoZ(char outStr[6]);

	void update();
};