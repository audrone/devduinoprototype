/**
 * \brief This class controls the display on the devduino
 */
#include "motor.hpp"
#include "sensors.hpp"

#define LINE0_Y 56
#define LINE1_Y 48
#define LINE2_Y 40
#define LINE3_Y 32
#define LINE4_Y 24
#define LINE5_Y 16
#define LINE6_Y 8
#define LINE7_Y 0

#define START_LINE_X 0

class LcdScreen 
{
	static LcdScreen* s_instance;
	Motor* motor_instance;
	Sensor* sensor_instance;

	LcdScreen();

  public:
	static LcdScreen* getInstance();

	void update();
};