#include <StandardCplusplus.h>
#include <devduino.h>

#define PIN_MOTOR_FL 9
#define PIN_MOTOR_FR 10
#define PIN_MOTOR_RL 11
#define PIN_MOTOR_RR 13

#define TOTAL_DECIMAL 5
#define AFTER_DECIMAL 1

class Motor
{
	static Motor *s_instance;

	float pwmFL;
	float pwmFR;
	float pwmRL;
	float pwmRR;

	Motor();
	int percentToDutyCycle(float pwm);

  public:
	static Motor *getInstance();

	void setupMotor();

	float getPwmFL();

	float getPwmFR();

	float getPwmRL();

	float getPwmRR();

	void getPwmFL(char outStr[6]);

	void getPwmFR(char outStr[6]);

	void getPwmRL(char outStr[6]);

	void getPwmRR(char outStr[6]);

	void setPwmFL(float pwm);

	void setPwmFR(float pwm);

	void setPwmRL(float pwm);

	void setPwmRR(float pwm);
};